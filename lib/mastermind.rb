class Code
  attr_reader :pegs

  PEGS = {
    R: "red",
    G: "green",
    B: "blue",
    Y: "yellow",
    O: "orange",
    P: "purple"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    pegs = string.chars.map(&:upcase).map(&:to_sym)

    raise "Code should have 4 pegs." if pegs.length != 4
    raise "Invalid colors in code" if !pegs.all? { |ch| PEGS.key?(ch) }

    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.keys.sample }
    Code.new(pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(code)
    exact_matches = 0
    4.times { |i| exact_matches += 1 if self[i] == code[i] }
    exact_matches
  end

  def near_matches(code)
    all_matches = 0

    PEGS.keys.each do |k|
      all_matches += [self.pegs.count(k), code.pegs.count(k)].min
    end

    all_matches - exact_matches(code)
  end

  def ==(code)
    return false unless code.is_a?(Code)
    pegs == code.pegs
  end

  def display
    pegs.map(&:to_s).join
  end

end

class Game
  attr_reader :secret_code
  attr_accessor :guess, :turns

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @turns = 0
  end

  def get_guess
    print "Guess a 4 color code (colors: RGBYOP): "
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

  def over?
    won? || turns == 12
  end

  def won?
    guess == secret_code
  end

  def conclude
    if won?
      "Congratulations! The secret code was #{secret_code.display}."
    else
      "Out of turns! Game over! The secret code was #{secret_code.display}."
    end
  end

  def play
    until over?
      @turns += 1
      puts "Turn #{turns} of 12"
      puts "--------"
      @guess = get_guess
      display_matches(guess)
      puts "--------"
    end

    puts conclude
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
